#include <iostream>
#include <math.h>

class PolarGenerator
{
private:
    bool nincsTarolt;
    double tarolt;
public:
    PolarGenerator();
    double kovetkezo();
};
PolarGenerator::PolarGenerator()
{
    this->nincsTarolt = true;
}
double PolarGenerator::kovetkezo()
{
    if (!this->nincsTarolt)
    {
        double u1, u2, v1, v2, w;
        do
        {
            // u1 = Math.random();
            // u2 = Math.random();
            u1 = ((double)(rand() % 1000)) / 1000.0;
            u2 = ((double)(rand() % 1000)) / 1000.0;
            v1 = 2 * u1 - 1;
            v2 = 2 * u2 - 1;
            w = v1 * v1 + v2 * v2;
        } while (w > 1);
        double r = sqrt((-2 * log(w)) / w);
        this->tarolt = r * v2;
        this->nincsTarolt = !this->nincsTarolt;
        return r * v1;
    }
    else
    {
        this->nincsTarolt = !this->nincsTarolt;
        return tarolt;
    }
}
void veryBad()
{
    PolarGenerator *otherGenerator = new PolarGenerator();
    otherGenerator->kovetkezo();
    delete otherGenerator;
    PolarGenerator polarGenerator;
    polarGenerator.kovetkezo();
}
int main(void)
{
    PolarGenerator polarGenerator;
    polarGenerator.kovetkezo();
    PolarGenerator *otherGenerator = new PolarGenerator();
    otherGenerator->kovetkezo();
    delete otherGenerator;
    for (int i = 0; i < 10; i++)
    {
        std::cout << polarGenerator.kovetkezo() << std::endl;
    }
    return 0;
}
