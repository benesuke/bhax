#include <stdio.h>

int 
main()
{
	int szam, db=0;
	printf("Adj meg egy decimális számot!\n");
	scanf("%d", &szam);
	printf("Átváltva:\n");
	for (int i = 0; i < szam; i++)
	{
		printf("|");
		db++;
		if (db % 10 == 0) 
                {
                printf(" ");
                }
	}
	printf("\n");
	return 0;
}

