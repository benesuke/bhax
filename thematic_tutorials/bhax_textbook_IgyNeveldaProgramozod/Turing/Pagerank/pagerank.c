#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void kiir(double tomb[], int db){
	for (int i=0;i<db;i++)
		printf("A pagerank: %lf \n",tomb[i]);

}

double hossz(double PR[],double PRv[],int n){
	double a = 0;
	for (int i=0;i<n;i++)
		a=a+abs(PR[i]-PRv[i]);
	return a;
}

void pager(double L[4][4]){
	double PR[4] = { 0.0, 0.0, 0.0, 0.0};
	double PRv[4] = {1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0};

	for (;;){
		for (int i=0;i<4;i++){
        		PR[i] = 0.0;
            	for (int j=0;j<4;j++)
                	PR[i]+=(L[i][j]*PRv[j]);

    	}

    	if (hossz(PR, PRv,4)<0.00000001) break;

    	for (int i=0;i<4;i++)
        	PRv[i]=PR[i];

	}

	kiir(PR,4);
}

int main(){
	double L[4][4] = {
	{0.0, 0.0, 1.0/3.0, 0.0},
	{1.0, 1.0/2.0, 1.0/3.0, 1.0},
	{0.0, 1.0/2.0, 0.0, 0.0},
	{0.0, 0.0, 1.0/3.0, 0.0}
};
pager(L);
}

