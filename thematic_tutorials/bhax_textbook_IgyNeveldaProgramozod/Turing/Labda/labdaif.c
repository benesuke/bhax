#include <curses.h>
#include <unistd.h>

int main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int a = 0;
    int b = 0;

    int amegy = 1;
    int bmegy = 1;

    int am;
    int bm;

    for ( ;; ) {

        getmaxyx ( ablak, bm , am );

        mvprintw ( b, a, "O" );

        refresh ();
        usleep ( 100000 );
        
        clear();

        a = a + amegy;
        b = b + bmegy;

        if ( a>=am-1 ) {
            amegy = amegy * -1;
        }
        if ( a<=0 ) {
            amegy = amegy * -1;
        }
        if ( b<=0 ) {
            bmegy = bmegy * -1;
        }
        if ( b>=bm-1 ) {
            bmegy = bmegy * -1;
        }
    }
    return 0;
}
